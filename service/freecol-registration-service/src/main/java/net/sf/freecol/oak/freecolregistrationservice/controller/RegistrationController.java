package net.sf.freecol.oak.freecolregistrationservice.controller;


import net.sf.freecol.oak.freecolregistrationservice.request.RegisterRequest;
import net.sf.freecol.oak.freecolregistrationservice.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class RegistrationController {

    private RegistrationService registrationService;

    @Autowired
    public RegistrationController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @PostMapping("/register-user")
    public String saveFreeColUser(@RequestBody RegisterRequest registerRequest) {
        return registrationService.registerNewUserFreeColUser(registerRequest);
    }

    @GetMapping("/confirm")
    public String confirm(@RequestParam("token") String token) {
        return registrationService.confirmToken(token);
    }
}
