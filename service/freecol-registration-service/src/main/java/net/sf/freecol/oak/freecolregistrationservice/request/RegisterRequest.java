package net.sf.freecol.oak.freecolregistrationservice.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class RegisterRequest {

    private String username;
    private String password;
    private String email;
    private String role;

}
