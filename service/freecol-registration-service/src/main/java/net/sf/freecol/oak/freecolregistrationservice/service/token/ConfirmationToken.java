package net.sf.freecol.oak.freecolregistrationservice.service.token;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import net.sf.freecol.oak.freecolregistrationservice.model.FreeColUser;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class ConfirmationToken {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String token;

    @Column(nullable = false)
    private LocalDateTime createdAt;
    private LocalDateTime expiresAt;
    private LocalDateTime confirmedAt;

    @ManyToOne
    @JoinColumn(name = "free_col_user_id")
    private FreeColUser freeColUser;

    public ConfirmationToken(String token, LocalDateTime createdAt, LocalDateTime expiresAt,  FreeColUser freeColUser) {
        this.token = token;
        this.createdAt = createdAt;
        this.expiresAt = expiresAt;
        this.freeColUser = freeColUser;
    }
}
