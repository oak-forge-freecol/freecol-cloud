package net.sf.freecol.oak.freecolregistrationservice.repository;

import net.sf.freecol.oak.freecolregistrationservice.model.FreeColUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository

public interface FreeColUserRepository extends JpaRepository<FreeColUser, Long> {

    Optional<FreeColUser> findByAndEmail(String email);

    @Transactional
    @Modifying
    @Query("UPDATE FreeColUser a " +
            "SET a.enabled = TRUE WHERE a.email = ?1")
    int enableFreeColUser(String email);
}
