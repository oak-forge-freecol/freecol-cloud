package net.sf.freecol.oak.freecolregistrationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class FreecolRegistrationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FreecolRegistrationServiceApplication.class, args);
	}

}
