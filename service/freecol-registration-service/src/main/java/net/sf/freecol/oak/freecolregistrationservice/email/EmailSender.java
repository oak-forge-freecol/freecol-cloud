package net.sf.freecol.oak.freecolregistrationservice.email;

public interface EmailSender {
    void send(String to, String email);
}
