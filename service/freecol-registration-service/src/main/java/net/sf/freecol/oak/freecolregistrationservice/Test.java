package net.sf.freecol.oak.freecolregistrationservice;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/test")
@RestController
public class Test {


    @GetMapping
    public String getHello() {
        return "hello from registration service";
    }
}
