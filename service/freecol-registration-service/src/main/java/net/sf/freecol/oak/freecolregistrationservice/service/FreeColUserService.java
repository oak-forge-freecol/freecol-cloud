package net.sf.freecol.oak.freecolregistrationservice.service;

import lombok.AllArgsConstructor;
import net.sf.freecol.oak.freecolregistrationservice.model.FreeColUser;
import net.sf.freecol.oak.freecolregistrationservice.repository.FreeColUserRepository;
import net.sf.freecol.oak.freecolregistrationservice.service.token.ConfirmationToken;
import net.sf.freecol.oak.freecolregistrationservice.service.token.ConfirmationTokenService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
@AllArgsConstructor
public class FreeColUserService {

    private FreeColUserRepository freeColUserRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private ConfirmationTokenService confirmationTokenService;

    public String signUpFreeColUser(FreeColUser freeColUser) {
        boolean userExists = freeColUserRepository
                .findByAndEmail(freeColUser.getEmail())
                .isPresent();

        if (userExists) {
            throw new IllegalStateException("email already taken");
        }

        String encodePassword = bCryptPasswordEncoder
                .encode(freeColUser.getPassword());

        freeColUser.setPassword(encodePassword);

        freeColUserRepository.save(freeColUser);


        String token = UUID.randomUUID().toString();

        ConfirmationToken confirmationToken = new ConfirmationToken(
                token,
                LocalDateTime.now(),
                LocalDateTime.now().plusMinutes(15),
                freeColUser
        );

        confirmationTokenService.saveConfirmationToken(confirmationToken);

        return token;
    }

    public int enableFreeColUser(String email) {
        return freeColUserRepository.enableFreeColUser(email);
    }
}
