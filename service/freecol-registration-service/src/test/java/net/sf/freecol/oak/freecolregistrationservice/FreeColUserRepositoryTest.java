package net.sf.freecol.oak.freecolregistrationservice;


import net.sf.freecol.oak.freecolregistrationservice.model.FreeColUser;
import net.sf.freecol.oak.freecolregistrationservice.repository.FreeColUserRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(properties = {
        "CERTIFICATE_PASSWORD = 123456",
        "SPRING_PROFILES_ACTIVE = test"
})
public class FreeColUserRepositoryTest {

    @Autowired
     FreeColUserRepository freeColUserRepository;

    @Test
    public void itShouldSaveFreeColUser() {

        //Given
        FreeColUser freeColUser = new FreeColUser();
        freeColUser.setUserName("userOne");
        freeColUser.setPassword("testOne");
        freeColUser.setEnabled(true);
        freeColUser.setEmail("testone@test.com");
        freeColUser.setRole("USER");

        //When
        freeColUserRepository.save(freeColUser);

        //Then
        assertNotNull(freeColUser);
        assertThat(freeColUser.getId()).isGreaterThan(0);

    }

    @Test
    public void itShouldFindUserByEmail() {

        //Given
        FreeColUser freeColUser = new FreeColUser();
        freeColUser.setUserName("testTwo");
        freeColUser.setPassword("testTwo");
        freeColUser.setEnabled(true);
        freeColUser.setEmail("testtwo@test.com");
        freeColUser.setRole("USER");

        //When
        freeColUserRepository.save(freeColUser);
        FreeColUser freeColUserTwo = freeColUserRepository.findByAndEmail("testtwo@test.com").get();

        //Then
        assertEquals(freeColUser.getUserName(), freeColUserTwo.getUserName());

    }

}