package net.sf.freecol.oak.freecolregistrationservice;

import net.sf.freecol.oak.freecolregistrationservice.request.RegisterRequest;
import net.sf.freecol.oak.freecolregistrationservice.service.RegistrationService;
import net.sf.freecol.oak.freecolregistrationservice.service.token.ConfirmationToken;
import net.sf.freecol.oak.freecolregistrationservice.service.token.ConfirmationTokenRepository;
import net.sf.freecol.oak.freecolregistrationservice.service.token.ConfirmationTokenService;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;

import org.junit.Test;



@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(properties = {
        "CERTIFICATE_PASSWORD = 123456",
        "SPRING_PROFILES_ACTIVE = test"
})
@ActiveProfiles("test")
public class RegistrationServiceTest {

    @Autowired
    RegistrationService registrationService;

    @Autowired
    ConfirmationTokenService confirmationTokenService;

    @Autowired
    ConfirmationTokenRepository confirmationTokenRepository;


    @Test
    public void it_should_return_confirmed() throws Exception {
        RegisterRequest registerRequest = new RegisterRequest("TestUser", "TestPassword", "tests@tests.com", "USER");

        String token = registrationService.registerNewUserFreeColUser(registerRequest);

        String result = registrationService.confirmToken(token);

        Assertions.assertEquals(result, "confirmed");
    }

    @Test(expected = IllegalStateException.class)
    public void when_not_found_token_it_should_throw_exception() throws Exception {
        String token = "not found token";
        String result = registrationService.confirmToken(token);

    }

    @Test(expected = IllegalStateException.class)
    public void when_token_expired_it_should_throw_exception() throws Exception {
        RegisterRequest registerRequest = new RegisterRequest("userOne", "testOne", "testone@test.com", "USER");
        String token = registrationService.registerNewUserFreeColUser(registerRequest);

        ConfirmationToken c = confirmationTokenService.getToken(token).orElseThrow();

        c.setExpiresAt(LocalDateTime.now());
        confirmationTokenRepository.save(c);

        String result = registrationService.confirmToken(token);

    }

    @Test(expected = IllegalStateException.class)
    public void when_token_already_confirmed_it_should_throw_exception() throws Exception {
        RegisterRequest registerRequest = new RegisterRequest("userTwo", "testTwo", "testtwo@test.com", "USER");
        String token = registrationService.registerNewUserFreeColUser(registerRequest);

        ConfirmationToken c = confirmationTokenService.getToken(token).orElseThrow();

        c.setConfirmedAt(LocalDateTime.now());
        confirmationTokenRepository.save(c);

        String result = registrationService.confirmToken(token);

    }

}
