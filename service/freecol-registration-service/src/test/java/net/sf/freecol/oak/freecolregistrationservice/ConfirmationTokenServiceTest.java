package net.sf.freecol.oak.freecolregistrationservice;


import net.sf.freecol.oak.freecolregistrationservice.model.FreeColUser;
import net.sf.freecol.oak.freecolregistrationservice.repository.FreeColUserRepository;
import net.sf.freecol.oak.freecolregistrationservice.service.token.ConfirmationToken;
import net.sf.freecol.oak.freecolregistrationservice.service.token.ConfirmationTokenRepository;
import net.sf.freecol.oak.freecolregistrationservice.service.token.ConfirmationTokenService;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(properties = {
        "CERTIFICATE_PASSWORD = 123456",
        "SPRING_PROFILES_ACTIVE = test"
})
@ActiveProfiles("test")
public class ConfirmationTokenServiceTest {


    @Autowired
    ConfirmationTokenService confirmationTokenService;

    @Autowired
    ConfirmationTokenRepository confirmationTokenRepository;

    @Autowired
    FreeColUserRepository freeColUserRepository;


    @Test
    public void it_should_save_confirmation_token() {

        FreeColUser freeColUser = new FreeColUser();

        freeColUser.setUserName("TestUsername");
        freeColUser.setPassword("TestPassword");
        freeColUser.setEnabled(false);
        freeColUser.setEmail("test@test.com");
        freeColUser.setRole("USER");

        freeColUserRepository.save(freeColUser);

        ConfirmationToken confirmationToken = new ConfirmationToken();
        confirmationToken.setToken("test-token");
        confirmationToken.setCreatedAt(LocalDateTime.now());
        confirmationToken.setExpiresAt(LocalDateTime.now().plusMinutes(15));
        confirmationToken.setFreeColUser(freeColUser);

        confirmationTokenService.saveConfirmationToken(confirmationToken);

        Assertions.assertNotNull(confirmationToken);
        assertThat(confirmationToken.getId()).isGreaterThan(0);


    }

    @Test
    public void it_should_confirmed_token() {

        FreeColUser freeColUser = new FreeColUser();

        freeColUser.setUserName("TestUsername");
        freeColUser.setPassword("TestPassword");
        freeColUser.setEnabled(false);
        freeColUser.setEmail("test@test.com");
        freeColUser.setRole("USER");

        freeColUserRepository.save(freeColUser);

        ConfirmationToken confirmationToken = new ConfirmationToken();
        confirmationToken.setToken("test-token");
        confirmationToken.setCreatedAt(LocalDateTime.now());
        confirmationToken.setExpiresAt(LocalDateTime.now().plusMinutes(15));
        confirmationToken.setFreeColUser(freeColUser);

        confirmationTokenService.saveConfirmationToken(confirmationToken);

        confirmationTokenService.setConfirmedAt(confirmationToken.getToken());

        ConfirmationToken tokenFromRepository = confirmationTokenRepository.findByToken("test-token").get();

        assertNotNull(tokenFromRepository.getConfirmedAt());

    }

}
