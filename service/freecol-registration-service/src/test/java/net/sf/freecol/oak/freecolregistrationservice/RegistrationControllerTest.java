package net.sf.freecol.oak.freecolregistrationservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.sf.freecol.oak.freecolregistrationservice.controller.RegistrationController;
import net.sf.freecol.oak.freecolregistrationservice.request.RegisterRequest;
import net.sf.freecol.oak.freecolregistrationservice.service.RegistrationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;



@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles(profiles = "test")
@WebMvcTest(controllers = RegistrationController.class)
@TestPropertySource(properties = {
        "CERTIFICATE_PASSWORD = 123456",
        "SPRING_PROFILES_ACTIVE = test"
})
public class RegistrationControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    RegistrationService registrationService;

    @Test
    public void itShouldReturnStatusOfCreatedUser() throws Exception {

        //Given&When&Then
        RegisterRequest registerRequest = new RegisterRequest("TestUser", "TestPassword", "test.com", "USER");

        this.mockMvc.perform(post("/register-user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(registerRequest)))
                .andDo(print())
                .andExpect(status().isOk());

    }

    @Test
    public void itShouldReturnConfirmedAccountByToken() throws Exception {

        //Given&When&Then
        String token = "3808f65a-0fc6-477a-8ee5-bec0ebe58eba";

        this.mockMvc.perform(get("/confirm")
                .param("token", token))
                .andDo(print())
                .andExpect(status().isOk());
    }

}