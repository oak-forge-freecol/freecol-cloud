package net.sf.freecol.oak.freecolauthservice;


import net.sf.freecol.oak.freecolauthservice.model.User;
import net.sf.freecol.oak.freecolauthservice.repository.UserRepository;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;



import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource(properties = {
        "CERTIFICATE_PASSWORD = 123456",
        "SPRING_PROFILES_ACTIVE = test"
})
@ActiveProfiles("test")
public class LoginTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    final String loginUser = "{\"username\": \"user\", \"password\": \"user\"}";
    final String loginAdmin = "{\"username\": \"admin\", \"password\": \"admin\"}";
    User user = new User();
    User admin = new User();


    @Test
    void shouldLoginAndGetContentForUser() throws Exception {
        user.setUsername("user");
        user.setPassword("$2a$12$.zsGOIAAWsrpDo5sCvHqsuS6/h4yLZi/lQC5w1GU2B7e7kMpcs.0S");
        user.setRoles("ROLE_USER");
        userRepository.save(user);

        MvcResult login = mockMvc.perform(post("/login")
                        .content(loginUser)
                )
                .andDo(print())
                .andExpect(status().is(200))
                .andReturn();
    }

    @Test
    void shouldLoginAndGetContentForAdmin() throws Exception {
        admin.setUsername("admin");
        admin.setPassword("$2a$12$3KTws3ZdfFRbqS/qXCssGOKeLeORK/Gy/tzSfh4e3ORl7CPUkRxma");
        admin.setRoles("ROLE_ADMIN");
        userRepository.save(admin);

        MvcResult login = mockMvc.perform(post("/login")
                        .content(loginAdmin)
                )
                .andDo(print())
                .andExpect(status().is(200))
                .andReturn();
    }

    @Test
    void shouldNotLoginWithNotExistingUser() throws Exception {
        String loginUser = "{\"username\": \"user2\", \"password\": \"password\"}";
        MvcResult login = mockMvc.perform(post("/login")
                        .content(loginUser)
                )
                .andDo(print())
                .andExpect(status().is(401))
                .andReturn();
    }

    @Test
    void shouldNotLoginWithIncorrectPassword() throws Exception {
        user.setUsername("user3");
        /*
        encrypted password "user3"
         */
        user.setPassword("$2a$12$CQFJCRXWrFGRkjYLEP48fuBK4lM/ZPfPRCdiSFM759NaNzMRqlM8K");
        user.setRoles("ROLE_USER");
        userRepository.save(user);

        String loginUser = "{\"username\": \"user3\", \"password\": \"password\"}";

        MvcResult login = mockMvc.perform(post("/login")
                        .content(loginUser)
                )
                .andDo(print())
                .andExpect(status().is(401))
                .andReturn();
    }
}

