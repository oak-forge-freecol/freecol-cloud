package net.sf.freecol.oak.freecolauthservice.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
public class AuthenticationController {

    @Value("${my.greeting: default value}")
    private String greetingMsg;

    @GetMapping("/hello")
    public String hello() {
        return "Hello from auth service, greeting msg: " + greetingMsg;
    }
}
