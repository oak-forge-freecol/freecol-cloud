package net.sf.freecol.oak.freecolauthservice;

import com.netflix.discovery.DiscoveryClient;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import javax.net.ssl.SSLContext;

@Configuration
public class SslDiscovery {

    @Value("${CERTIFICATE_PASSWORD}")
    private String trustStorePassword;

    @Bean
    public DiscoveryClient.DiscoveryClientOptionalArgs getTrustStoredEurekaClient(SSLContext sslContext) {
        DiscoveryClient.DiscoveryClientOptionalArgs args = new DiscoveryClient.DiscoveryClientOptionalArgs();
        args.setSSLContext(sslContext);
        return args;
    }

    @Bean
    public SSLContext sslContext() throws Exception {
        final ClassPathResource resourceFile = new ClassPathResource("auth-service.p12");
        return new SSLContextBuilder()
                .loadTrustMaterial(
                        resourceFile.getURL(),
                        trustStorePassword.toCharArray()
                ).build();
    }
}
