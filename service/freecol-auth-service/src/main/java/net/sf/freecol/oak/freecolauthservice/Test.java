package net.sf.freecol.oak.freecolauthservice;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class Test {
    //delete later, created for tests
    @GetMapping
    public String getTest() {
        return "Hello from auth service";
    }
}
