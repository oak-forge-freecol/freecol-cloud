package net.sf.freecol.oak.freecolauthservice.security;


import net.sf.freecol.oak.freecolauthservice.model.FreeColUserDetails;
import net.sf.freecol.oak.freecolauthservice.model.User;
import net.sf.freecol.oak.freecolauthservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class MyUserDetailsImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
        Optional<User> user = userRepository.findByUsername(username);

        user.stream().map(m->m.getRoles()).forEach(System.out::println);

        return user.map(FreeColUserDetails::new).get();
    }
}

