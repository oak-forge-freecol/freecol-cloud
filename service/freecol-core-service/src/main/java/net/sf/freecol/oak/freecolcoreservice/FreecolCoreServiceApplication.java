package net.sf.freecol.oak.freecolcoreservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class FreecolCoreServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(FreecolCoreServiceApplication.class, args);
    }

}
