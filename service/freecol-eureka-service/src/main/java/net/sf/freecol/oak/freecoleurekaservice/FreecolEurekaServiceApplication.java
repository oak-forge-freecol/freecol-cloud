package net.sf.freecol.oak.freecoleurekaservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class FreecolEurekaServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(FreecolEurekaServiceApplication.class, args);
    }

}