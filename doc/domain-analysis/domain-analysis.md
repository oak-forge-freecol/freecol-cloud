**During the project we will not adopt generally available DDD principles, but will revise them according to our needs. As Message Broker we will use RabbitMQ.**

For this moment, the project is divided into the following business objects :

**1. Creation of Game Session** :

- Process of creation GameSession
- Process of successfully saving GameSession
- Process of successfully joining GameSession by Game owner
- Process of unsuccessfully joining GameSession by Game owner
- Process of unsuccessfully saving GameSession
- Process of not being able to create GameSession

**2. Destruction of Game Session** :

- Process of Destruction of Game Session
- Process in case of all players are disconnected
- Process in case of not all players are disconnected

**3. Starting Gameplay**

- Process in case of required number of players is connected
- Process in case of required number of players is not connected

**4. Logout / Losing connection / Rejoining**
- Process is the same for each of the scenarios

<br>

The project adopts the following layers which is shown hierarchically starting from the top :

- Domain
- Aggregate
- Handler

<br>

Below there are diagrams and their descriptions for each identified business object, although it has an informative value and can be modified in a convenient way.

-------------------------------------------------------------------------------------
<br>
<br>
<br>

**CREATION OF GAME SESSION**

![image](images/creation_of_game_session.png)

Objects contained in this domain:

- Client
- MessageBus
- GameSessionAggregate
- GameSessionInitializationHandler
- Repository
- GameSessionJoinHandler
- GameSessionDestructionHandler
- GameSession

**1. Process of creation GameSession**

1. Command to create the GameSession is provided sequentially from user through :

- Client
- MessageBus
- GameSessionAggregate
- GameSessionInitializationHandler

2. MessageBus waits for event from GameSessionInitializationHandler which confirms that there is a GameSession active for the user
3. MessageBus provides this event to Repository
4. MessageBus waits for event from Repository which confirms that there is no active GameSession
5. MessageBus provides this event to GameSessionInitializationHandler
6. GameSessionInitializationHandler creates GameSession
7. MessageBus waits for event from GameSessionInitializationHandler which confirms that GameSession persists
8. MessageBus provides this event to Repository

**2. Process of successfully saving GameSession**

1. MessageBus waits for event from repository which confirms that GameSession persists
2. MessageBus provides this event to GameSessionInitializationHandler
3. MessageBus waits for event from GameSessionInitializationHandler regarding joining GameSession
4. MessageBus provides this event to GameSessionJoinHandler

**3. Process of successfully joining GameSession by Game owner**

1. MessageBus waits for event from GameSessionJoinHandler which confirms that the Game owner has joined Game Session
2. MessageBus provides this event to GameSessionInitializationHandler
3. MessageBus waits for event from GameSessionInitializationHandler which confirms that GameSession is initialized
4. MessageBus provides this event to Client
5. Client goes to lobby view

**4. Process of unsuccessfully joining GameSession by Game owner**

1. GameSessionJoinHandler starts process of retry joining player (one more attempt of process of successfully joining GameSession by GameOwner)
2. MessageBus waits for event from GameSessionJoinHandler which confirms that joining game wasn't possible
3. MessageBus provides this event to GameSessionInitializationHandler
4. MessageBus waits for event from GameSessionJoinHandler regarding destroy GameSession
5. MessageBus waits for event from GameSessionJoinHandler regarding no possibility of create GameSession
6. MessageBus provides this event to Client
7. MessageBus provides event regarding destroy GameSession to GameSessionDestructionHandler

**5. Process of unsuccessfully saving GameSession**

1. MessageBus waits for event from repository regarding GameSession persisting error
2. This event is provided sequentially from MessageBus through :
- GameSessionInitializationHandler
- GameSessionAggregate
3. MessageBus waits for event from GameSessionAggregate which confirms that GameSession cannot be created
4. MessageBus provides this event to Client
5. Client informs the user about GameSession creation failed

**6. Process of not being able to create GameSession**

1. MessageBus waits for event from Repository which confirms that active GameSession is present
2. MessageBus provides this event to GameSessionInitializationHandler
3. Info that cannot create GameSession is provided sequentially from GameSessionInitializationHandler through :

- GameSessionAggregate
- MessageBus
- Client
- User

<br>
<br>
<br>

**DESTRUCTION OF GAME SESSION**

![image](images/destruction_of_game_session.png)

Objects contained in this domain:

- Client
- Messagebus
- GameSessionAggregate
- GameSessionDestructionHandler
- Repository
- GameSessionConnectionsHandler

**1. Process of Destruction of Game Session**

1. Event regarding destruction of GameSession is provided sequentially from Client through:

- MessageBus
- GameSessionAggregate
- GameSessionDestructionHandler

2. GameSessionDestructionHandler sends event (question) if all players are disconnected to MessageBus
3. MessageBus provides this event to Repository

**2. Process in case of all players are disconnected**

1. MessageBus waits for event from Repository regarding all players are disconnected
2. MessageBus provides this event to GameSessionDestructionHandler
3. MessageBus waits for event from GameSessionDestructionHandler to delete GameSession
4. MessageBus provides this event to Repository
5. MessageBus waits for event from Repository whether the GameSession is deleted
6. MessageBus provides this event to GameSessionDestructionHandler
7. Event regarding complete destruction of GameSession is provided sequentially from GameSessionDestructionHandler through :

- MessageBus
- Client

**3. Process in case of not all players are disconnected**

1. MessageBus waits for event from Repository regarding not all players are disconnected
2. MessageBus provides this event to GameSessionDestructionHandler
3. MessageBus waits for event from GameSessionDestructionHandler to close all connection for GameSession
4. MessageBus provides this event to GameSessionConnectionsHandler
5. MessageBus waits for event from GameSessionConnectionsHandler which confirms that all connections are closed
6. MessageBus provides this event to GameSessionDestructionHandler
7. MessageBus waits for event from GameSessionDestructionHandler to delete GameSession
8. MessageBus provides this event to Repository
9. MessageBus waits for event form Repository which confirms that GameSession is deleted
10. MessageBus provides this event to GameSessionDestructionHandler
11. Event regarding complete destruction of GameSession is provided sequentially from GameSessionDestructionHandler through :

- MessageBus
- Client

<br>
<br>
<br>

**STARTING GAMEPLAY**

![image](images/starting_gameplay.png)

Objects contained in this domain:

- Client
- MessageBus
- GameSessionAggregate
- GameSessionInitializationHandler
- Repository
- GameplayStartHandler
- GameSessionConnectionsHandler
- GameplayGeneratorHandler


1. Event regarding starting gameplay is provided sequentially from Client through :

- MessageBus
- GameSessionAggregate

2. GameSessionAggregate provides info to GameSessionInitializationHandler regarding init GameSession
3. MessageBus waits for event from GameSessionInitializationHandler which confirms that GameSession is initialized
4. MessageBus provides this event to GameplayStartHandler
5. MessageBus waits for event (question) from GameplayStartHandler regarding required number of players connected
6. MessageBus provides this event (question) to GameSessionConnectionsHandler

**1. Process in case of required number of players is connected**

1. MessageBus waits for event from GameSessionConnectionsHandler which confirms that required number of players is connected
2. MessageBus provides this event to GameplayStartHandler
3. MessageBus waits for event from GameplayStartHandler to generate Gameplay
4. MessageBus provides this event to GameplayGeneratorHandler
5. MessageBus waits for event from GameplayGeneratorHandler to get Gameplay Settings
6. MessageBus provides this event to Repository
7. MessageBus waits for event from Repository to return Gameplay Settings
8. MessageBus provides this event to GameplayGeneratorHandler
9. GameplayGeneratorHandler generates Gameplay
10. MessageBus waits for event from GameplayGeneratorHandler which confirms that Gameplay is generated
11. MessageBus provides this event to GameplayStartHandler
12. GameplayStartHandler starts Gameplay
13. Event regarding started Gameplay is provided sequentially from GameplayStartHandler through :

- MessageBus
- Client

**2. Process in case of required number of players is not connected**

1. MessageBus waits for event from GameSessionConnectionsHandler which confirms that not enough players are connected
2. MessageBus provides this event to GameplayStartHandler
3. GameplayStartHandler starts waiting for players
4. MessageBus waits for event from GameplayStartHandler which confirms that all players are joined
5. MessageBus waits for event from GameplayStartHandler to generate Gameplay
6. MessageBus waits for event from GamePlayGeneratorHandler to get Gameplay Settings
7. MessageBus provides this event to Repository
8. MessageBus waits for event from Repository to return Gameplay Settings
9. MessageBus provides this event to GameplayGeneratorHandler
10. GameplayGeneratorHandler generates Gameplay
11. MessageBus waits for event from GameplayGeneratorHandler which confirms that Gameplay is generated
12. MessageBus provides this event to GameplayStartHandler
13. GameplayStartHandler starts Gameplay
14. Event regarding started Gameplay is provided sequentially from GameplayStartHandler through :

- MessageBus
- Client


<br>
<br>
<br>

**LOGOUT / LOOSING CONNECTION / RE-JOINING**

![image](images/logout_loosing_connection_rejoining.png)

Objects contained in this domain:

- Client
- MessageBus
- losingConnectionAggregate
- losingConnectionHandler
- AIService

1. MessageBus waits for event from Client which confirms that the connection is lost
2. MessageBus provides this event to losingConnectionAggregate
3. Series of messages begins between the aggregate (LosingConnectionAggregate) and its handler (LosingConnectionHandler) :

- aggregate informs the handler to start the connection loss process
- handler informs the aggregate to create log that logout begins with player's name
- aggregate informs handler to complete logout of this client from the server
- handler informs aggregate to create log that logout ends with player's name and reason of logout

4. losingConnectionHandler informs the AIService to replace the player with AI
5. MessageBus waits for event from AIService which confirms that control is taken by AI instead of player
6. MessageBus provides this event to losingConnectionHandler
7. losingConnectionHandler informs LosingConnectionAggregate to create the log that AI takes control
8. MessageBus waits for event from Client which confirms that player starts to reconnect
9. MessageBus provides this event to losingConnectionAggregate
10. losingConnectionAggregate informs the losingConnectionHandler to start the reconnect process
11. losingConnectionHandler sends the two last messages :

- informs AIService to restore control to the player
- informs losingConnectionAggregate to create log that reconnect ends with player's name




